#!/bin/bash
#$ -cwd
#$ -N align_augmented_genome
#$ -l h_rt=48:00:00
#$ -l h_vmem=8G

## Align PK15 sequencing to an augmented genome

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/bwa/0.7.12-r1039
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4
module load igmm/apps/samtools/1.3


source scripts/set_tools_paths.sh


## Create augmented reference

if [ ! -f genomes/Sscrofa11.1_augmented.fa ]; then

    cat genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa sv40_gfp.fasta > \
	genomes/Sscrofa11.1_augmented.fa
    
    samtools faidx genomes/Sscrofa11.1_augmented.fa
    
    java -Xmx4g -jar $PICARD_HOME/picard.jar CreateSequenceDictionary \
	 R=genomes/Sscrofa11.1_augmented.fa \
	 O=genomes/Sscrofa11.1_augmented.dict

    bwa index -a bwtsw genomes/Sscrofa11.1_augmented.fa 

fi


bwa mem -M \
    genomes/Sscrofa11.1_augmented.fa \
    seq/fastq/PK15_genomic_DNA_R1.fastq.gz \
    seq/fastq/PK15_genomic_DNA_R2.fastq.gz \
    | samtools view -Sbh - \
	       > seq/bam/PK15_genomic_DNA_augmented.bam
