

library(dplyr)
library(readr)
library(ggplot2)
library(hexbin)
library(patchwork)
library(tidyr)


snps <- read_tsv("outputs/ips_snps_AD_freq.txt")

permissible_biallelic <- c("./.", "0/0", "0/1", "1/1")

snps_biallelic <- filter(snps,
                         genotype %in% permissible_biallelic)


snps_not_fixed_alt <- filter(snps_biallelic,
                             genotype != "1/1")


## Cross-tabulation of genotype calls

geno_tab_total <- table(snps_biallelic$genotype)

print(geno_tab_total/sum(geno_tab_total))



## Histograms of frequency

histogram_data <- pivot_longer(snps,
                               altfreq,
                               names_to = "variable",
                               values_to = "frequency")

histogram_data$sample <- "fibroblast"

plot_histogram <- ggplot() +
  geom_histogram(mapping = aes(x = frequency),
                 bins = 20,
                 data = histogram_data) +
  facet_wrap(~ factor(Chr_name, levels = c(1:18, "X", "Y")),
             scale = "free_y") +
  theme_bw() +
  theme(panel.grid = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Number of variants") +
  ggtitle("ALT allele frequency, fibroblast")



pdf("figures/allele_frequency_histogram_ips.pdf",
    width = 10)
print(plot_histogram)
dev.off()

