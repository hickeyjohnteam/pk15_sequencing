#!/bin/bash
#$ -cwd
#$ -N pk15_readgroups
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G

## Use picard to set read groups on the alignment

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh




java -Xmx4g -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
     I=seq/aln/EG_pk15.bam \
     O=seq/aln/EG_pk15_rg.bam \
     RGID=PK15_EG \
     RGLB=PK15_EG \
     RGPL=illumina \
     RGPU=PK15_EG \
     RGSM=PK15_EG \
     SORT_ORDER=coordinate \
     TMP_DIR=$TMPDIR


java -Xmx4g -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
     I=seq/aln/GW_pk15.bam \
     O=seq/aln/GW_pk15_rg.bam \
     RGID=PK15_GW \
     RGLB=PK15_GW \
     RGPL=illumina \
     RGPU=PK15_GW \
     RGSM=PK15_GW \
     SORT_ORDER=coordinate \
     TMP_DIR=$TMPDIR