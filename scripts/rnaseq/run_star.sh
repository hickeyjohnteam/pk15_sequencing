#!/bin/bash
#$ -cwd
#$ -N run_star
#$ -l h_rt=24:00:00
#$ -l h_vmem=32G


. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/STAR/2.7.8a

BASE_PATH=$(pwd)/
  
while read LINE; do

  STAR --genomeDir $BASE_PATH/rnaseq/star_index \
    --runThreadN 1 \
    --readFilesIn ../pk15_rnaseq/${FILE}_1.fastq.gz ../pk15_rnaseq/${FILE}_2.fastq.gz \
    --outFileNamePrefix rnaseq/star_$FILE \
    --readFilesCommand zcat \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMunmapped Within \
    --outSAMattributes Standard 
    
done < "metadata/fastq_files.txt"


