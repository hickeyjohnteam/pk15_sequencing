

cd ../pk15_rnaseq/


## Pig bodymap

curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR119/008/SRR11939208/SRR11939208_1.fastq.gz
curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR119/008/SRR11939208/SRR11939208_2.fastq.gz


## Mock infected PK15

curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/007/SRR6806317/SRR6806317_1.fastq.gz
curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/007/SRR6806317/SRR6806317_2.fastq.gz


curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/006/SRR6806316/SRR6806316_1.fastq.gz
curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/006/SRR6806316/SRR6806316_2.fastq.gz


curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/008/SRR6806318/SRR6806318_1.fastq.gz
curl -O ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR680/008/SRR6806318/SRR6806318_2.fastq.gz