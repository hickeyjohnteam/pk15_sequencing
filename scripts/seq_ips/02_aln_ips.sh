#!/bin/bash
#$ -cwd
#$ -N ips_aln
#$ -l h_rt=48:00:00
#$ -l h_vmem=2G
#$ -pe sharedmem 8

## Use bwa mem to align reads to the reference genome

. /etc/profile.d/modules.sh

set -ue

source scripts/set_tools_paths.sh

if [ ! -d seq/aln ]; then
    mkdir seq/aln
fi


module load igmm/apps/bwa/0.7.12-r1039
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4
module load igmm/apps/samtools/1.3

bwa mem -M \
    -t 8 \
    genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
    seq/trim/ips_1_paired.fastq.gz \
    seq/trim/ips_2_paired.fastq.gz \
    | samtools view -Sbh - \
	       > seq/aln/ips.bam
