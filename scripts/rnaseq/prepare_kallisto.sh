#!/bin/bash
#$ -cwd
#$ -N prepare_kallisto
#$ -l h_rt=8:00:00
#$ -l h_vmem=8G


. /etc/profile.d/modules.sh

set -ue

module load roslin/kallisto/0.44.0

## Create index for kallisto run

if [ ! -f annotation/Sus_scrofa.Sscrofa11.1.cdna.all.fa.gz ]; then
  cd annotation
  curl -O http://ftp.ensembl.org/pub/release-107/fasta/sus_scrofa/cdna/Sus_scrofa.Sscrofa11.1.cdna.all.fa.gz
  cd ..

fi


if [ ! -d rnaseq ]; then
  mkdir rnaseq
fi


kallisto index \
  -i rnaseq/transcripts.idx \
  annotation/Sus_scrofa.Sscrofa11.1.cdna.all.fa.gz