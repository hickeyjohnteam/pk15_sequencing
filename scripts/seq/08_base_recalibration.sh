#!/bin/bash
#$ -cwd
#$ -N pk15_EG_aln
#$ -l h_rt=200:00:00
#$ -l h_vmem=8G

## Use base recalibration to remove bias from quality scores

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/R/3.5.0

source scripts/set_tools_paths.sh

if [ ! -d seq/aln ]; then
    mkdir seq/aln
    
if


# Generate the first pass recalibration table file
 java -jar GenomeAnalysisTK.jar \
      -T BaseRecalibrator \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -I myinput.bam \
      -knownSites bundle/my-trusted-snps.vcf \ # optional but recommended
      -knownSites bundle/my-trusted-indels.vcf \ # optional but recommended
      -o firstpass.table
      
      
 # Generate the second pass recalibration table file
 java -jar GenomeAnalysisTK.jar \
      -T BaseRecalibrator \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -I myinput.bam \
      -knownSites bundle/my-trusted-snps.vcf \
      -knownSites bundle/my-trusted-indels.vcf \
      -BQSR firstpass.table \
      -o secondpass.table
      
      
 # Finally generate the plots and also keep a copy of the csv (optional)
 java -jar GenomeAnalysisTK.jar \
      -T AnalyzeCovariates \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -before firstpass.table \
      -after secondpass.table \
      -csv BQSR.csv \ # optional
      -plots BQSR.pdf