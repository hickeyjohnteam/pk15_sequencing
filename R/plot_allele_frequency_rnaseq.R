
## Plot allele frequencies from PK15 RNA-seq

library(assertthat)
library(dplyr)
library(readr)
library(ggplot2)
library(hexbin)
library(patchwork)
library(purrr)
library(tidyr)


snps <- read_tsv("outputs/pk15_rnaseq_snps_AD_freq.txt",
                 col_types = "cncccccccnnnnnnc")



## Restrict to biallelic
permissible_biallelic <- c("./.", "0/0", "0/1", "1/1")


snps_biallelic <- filter(snps,
                         bodymap_genotype %in% permissible_biallelic &
                           mock_genotype %in% permissible_biallelic)




## Remove fixed ALT
snps_not_fixed_alt <- filter(snps_biallelic,
                             !(bodymap_genotype == "1/1" | mock_genotype == "1/1"))



snps_not_fixed_alt <- filter(snps,
                             (bodymap_refcount >= 5 &
                               bodymap_altcount >= 5) |
                               (mock_refcount >= 5 &
                                  mock_altcount >= 5))


## Cross-tabulation of genotype calls

geno_crosstab_total <- table(snps_biallelic$bodymap_genotype,
                             snps_biallelic$mock_genotype)

print(geno_crosstab_total)

print(geno_crosstab_total/sum(geno_crosstab_total))




## Histograms of frequency

histogram_data <- pivot_longer(snps_not_fixed_alt[, c("Chromosome", "Position",
                                                      "bodymap_altfreq", "mock_altfreq")],
                               c(bodymap_altfreq, mock_altfreq),
                               names_to = "variable",
                               values_to = "frequency")

histogram_data$sample <- ifelse(histogram_data$variable == "bodymap_altfreq",
                                "PK15 BodyMap",
                                "PK15 mock transfected")

histogram_data <- filter(histogram_data, Chromosome %in% c(1:18, "X"))


plot_histograms <- ggplot() +
  geom_histogram(mapping = aes(x = frequency),
                 bins = 25,
                 data = histogram_data) +
  facet_wrap(~ factor(sample, levels = c("PK15 BodyMap", "PK15 mock transfected")),
             ncol = 1) +
  theme_bw() +
  theme(panel.grid = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Number of variants")



## Densities

plot_freq_densities <- qplot(x = frequency,
                             data = histogram_data,
                             geom = "density") +
  facet_grid(factor(sample, levels = c("PK15 BodyMap", "PK15 mock transfected")) ~ 
               factor(Chromosome, levels = c(1:18, "X")),
             scale = "free_y") +
  scale_x_continuous(breaks = c(1/2, 1)) +
  theme_bw() + theme(panel.grid = element_blank()) +
  theme_bw(base_size = 16) +
  theme(panel.grid = element_blank(),
        strip.background = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Density of variants") +
  ggtitle("Within-sample allele frequency per chromosome")


pdf("figures/rnaseq_allele_frequency_densities.pdf",
    width = 20)
print(plot_freq_densities)
dev.off()



plot_freq_densities_together <- qplot(x = frequency,
                                      data = filter(histogram_data, Chromosome != "Y"),
                                      geom = "density",
                                      group = Chromosome) +
  facet_wrap(~ factor(sample, levels = c("PK15 BodyMap", "PK15 mock transfected"))) +
  geom_vline(xintercept = c(1/3, 1/2, 2/3),
             linetype = 2, colour = "red") +
  theme_bw() +
  theme(panel.grid = element_blank(),
        strip.background = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Density of variants") +
  ggtitle("Within-sample allele frequencies per chromosome")



source("R/simulation_functions.R")

fraction_diploid <- c(0.2, 0.4, 0.6)


ploidy_mixes <- map(fraction_diploid,
                    function(f) {
                      tibble(f = c(f, 1-f),
                             ploidy = 2:3,
                             alt_ratios = list(1, c(1, 2)))
                    })

modes_mixes <- map(ploidy_mixes,
                   get_modes_df)

modes <- flatten_dbl(modes_mixes)



fraction_tetraploid <- c(0.1, 0.3, 0.5)

ploidy_mixes_tetra <- map(fraction_tetraploid,
                          function(f) {
                            tibble(f = c(1 - f, f),
                                   ploidy = 3:4,
                                   alt_ratios = list(c(1, 2), c(2)))
                          })

modes_mixes_tetra <- map(ploidy_mixes_tetra,
                         get_modes_df)

modes_tetra <- flatten_dbl(modes_mixes_tetra)


plot_freq_densities_trip <- qplot(x = frequency,
                                  data = filter(histogram_data,
                                                Chromosome %in% c(1, 2, 4, 6, 9, 13, 14) &
                                                  sample == "PK15 mock transfected"),
                                  geom = "density",
                                  group = Chromosome) +
  geom_vline(xintercept = c(1/3, 1/2, 2/3),
             linetype = 2, colour = "red") +
  geom_vline(xintercept = modes,
             linetype = 2, colour = "blue") +
  # geom_vline(xintercept = modes_tetra,
  #            linetype = 2, colour = "orange") +
  theme_bw() +
  theme(panel.grid = element_blank(),
        strip.background = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Density of variants") +
  ggtitle("Within-chromosome allele frequencies chrs 1, 2, 4, 6, 9, 13, 14\n(PK15 mock transfected)")


pdf("figures/rnaseq_allele_frequency_bodymap_with_modes.pdf",
    height = 3.5)
print(plot_freq_densities_trip)
dev.off()





plot_freq_densities_chr18 <- qplot(x = frequency,
                                   data = filter(histogram_data,
                                                 Chromosome == 18 &
                                                   sample == "PK15 mock transfected"),
                                   geom = "density",
                                   group = Chromosome) +
  geom_vline(xintercept = c(1/5, 4/5),
             linetype = 2, colour = "red") +
  theme_bw() +
  theme(panel.grid = element_blank(),
        strip.background = element_blank()) +
  xlab("ALT allele frequency") +
  ylab("Density of variants")
