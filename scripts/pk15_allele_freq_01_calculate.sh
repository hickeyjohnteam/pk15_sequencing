#!/bin/bash
#$ -cwd
#$ -N pk15_allele_freq_01_calculate.sh
#$ -l h_rt=0:10:00
#$ -l h_vmem=8G

. /etc/profile.d/modules.sh

set -ue


module load igmm/apps/R/3.5.0


Rscript R/calculate_pk15_allele_frequency.R
