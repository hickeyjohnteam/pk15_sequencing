#!/bin/bash
#$ -cwd
#$ -N pk15_GW_BQSR_gvcf
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 16

## Use GATK haplotype caller to create BQSR gvcf (using bam file from 08_BQSR)

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq/gvcf ]; then
    mkdir seq/gvcf
fi


java -Xmx100g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T HaplotypeCaller \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -I seq/base_recalibration/GW_BQSR.bam \
     --emitRefConfidence GVCF \
     -variant_index_type LINEAR \
     -variant_index_parameter 128000 \
     -nct 16 \
     -o seq/gvcf/GW_pk15.BQSF.g.vcf


