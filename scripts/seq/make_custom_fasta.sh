#!/bin/bash
#$ -cwd
#$ -N make_custom_fasta
#$ -l h_rt=8:00:00
#$ -l h_vmem=24G

## Make a custom fasta with GW SNPs

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66
module load igmm/libs/htslib/1.4


source scripts/set_tools_paths.sh

cd seq

if [ ! -d custom_fasta ]; then
  mkdir custom_fasta
fi

cp genotype_filtered_vcf/pk15_snps_filters_applied.vcf custom_fasta/pk15_bcftools.vcf
bgzip custom_fasta/pk15_bcftools.vcf
$BCFTOOLS_HOME/bcftools sort custom_fasta/pk15_bcftools.vcf.gz -O z > \
  custom_fasta/pk15_bcftools_sorted.vcf.gz
$BCFTOOLS_HOME/bcftools index custom_fasta/pk15_bcftools_sorted.vcf.gz 

##echo Normalizing
##
##$BCFTOOLS_HOME/bcftools norm \
##			-f ../genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
##			custom_fasta/pk15_bcftools_sorted.vcf.gz \
##			-Ob \
##			-o custom_fasts/pk15.norm.bcf

## $BCFTOOLS_HOME/bcftools index custom_fasta/pk15.norm.bcf

echo Consensus

$BCFTOOLS_HOME/bcftools consensus \
			--sample PK15_GW \
			-f ../genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
			-I \
			-H A \
			custom_fasta/pk15_bcftools_sorted.vcf.gz > custom_fasta/pk15_GW_custom_snps.fasta
