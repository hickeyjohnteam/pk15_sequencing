#!/bin/bash
#$ -cwd
#$ -N hampshire_download
#$ -l h_rt=8:00:00
#$ -l h_vmem=8G

## Download 4 Hampshire samples from ENA

. /etc/profile.d/modules.sh

set -ue


if [ ! -d seq/fastq_ena ]; then
    mkdir seq/fastq_ena
fi


cd seq/fastq_ena

curl -O ftp.sra.ebi.ac.uk/vol1/fastq/ERR173/ERR173174/ERR173174_1.fastq.gz
curl -O ftp.sra.ebi.ac.uk/vol1/fastq/ERR173/ERR173174/ERR173174_2.fastq.gz

curl -O ftp.sra.ebi.ac.uk/vol1/fastq/ERR173/ERR173175/ERR173175_1.fastq.gz
curl -O ftp.sra.ebi.ac.uk/vol1/fastq/ERR173/ERR173175/ERR173175_2.fastq.gz

curl -o ERR977248_1.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0030_SSWB21M02_8_1.fq.gz
curl -o ERR977248_2.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0030_SSWB21M02_8_2.fq.gz

curl -o ERR977249_1.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0031_SSWB21M02_8_1.fq.gz
curl -o ERR977249_2.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0031_SSWB21M02_8_2.fq.gz

curl -o ERR977250_1.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0032_SSHA20U01_5_1.fq.gz
curl -o ERR977250_2.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0032_SSHA20U01_5_2.fq.gz

curl -o ERR977251_1.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0033_SSHA20U01_8_1.fq.gz
curl -o ERR977251_2.fastq.gz ftp.sra.ebi.ac.uk/vol1/ERA463/ERA463003/fastq/ABGSA0033_SSHA20U01_8_2.fq.gz

echo Done