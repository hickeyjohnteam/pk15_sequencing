#!/bin/bash
#$ -cwd
#$ -N bcftools_rnaseq
#$ -l h_rt=1:00:00
#$ -l h_vmem=16G

. /etc/profile.d/modules.sh

set -ue

module load igmm/libs/htslib/1.4

source scripts/set_tools_paths.sh


$GATK4_HOME/gatk SelectVariants \
  -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
  -V rnaseq/vcf/pk15_combined_filtered.vcf.gz \
  --select-type-to-include SNP \
  --exclude-filtered \
  -O rnaseq/vcf/pk15_combined_snps_filtered_excluded.vcf.gz

$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
  rnaseq/vcf/pk15_combined_snps_filtered_excluded.vcf.gz > \
  outputs/pk15_rnaseq_snps_AD.txt

   
echo Done