#!/bin/bash
#$ -cwd
#$ -N hampshire_genome_coverage_windows
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G

## Get sequence coverage of windows of control genomes


. /etc/profile.d/modules.sh

set -eu

module load roslin/bedtools/2.26.0


for SAMPLE in SAMEA1557391 SAMEA1557400 SAMEA3497843 SAMEA3497844; do

    bedtools coverage \
        -a genomes/Sscrofa11.1_10kbp_windows.bed \
        -b seq/dedup/${SAMPLE}.bam \
        -sorted \
        > outputs/coverage/${SAMPLE}_coverage.txt

done

echo Done
