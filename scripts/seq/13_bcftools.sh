#!/bin/bash
#$ -N bcf
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=16G


## bcftools and htslib module (for bgzip)

BCFTOOLS_HOME=/exports/cmvm/eddie/eb/groups/tier2_hickey_group/InnovateUK/martinj/tools/bcftools-1.10.2/

. /etc/profile.d/modules.sh

module load igmm/libs/htslib/1.4

## Block zip, sort and index
bgzip seq/genotype_filtered_vcf/pk15_snps_BQSF_all.vcf
$BCFTOOLS_HOME/bcftools sort seq/genotype_filtered_vcf/pk15_snps_BQSF_all.vcf.gz \
-O z > seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz
$BCFTOOLS_HOME/bcftools index seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz

bgzip seq/genotype_filtered_vcf/pk15_indels_BQSF_all.vcf
$BCFTOOLS_HOME/bcftools sort seq/genotype_filtered_vcf/pk15_indels_BQSF_all.vcf.gz \
 -O z > seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz
$BCFTOOLS_HOME/bcftools index seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz

## Query for per-sample tags
$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz > \
outputs/pk15_snps_AD.txt

$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz > \
outputs/pk15_indels_AD.txt
