#!/bin/bash
#$ -cwd
#$ -N genotype_gvcf
#$ -l h_rt=48:00:00
#$ -l h_vmem=32G

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d rnaseq/vcf ]; then
  mkdir rnaseq/vcf
fi

$GATK4_HOME/gatk CombineGVCFs  \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   --variant rnaseq/gvcf/pk15_bodymap.g.vcf.gz \
   --variant rnaseq/gvcf/pk15_mock.g.vcf.gz \
   -O rnaseq/gvcf/pk15_combined.g.vcf.gz
   
$GATK4_HOME/gatk GenotypeGVCFs  \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -V rnaseq/gvcf/pk15_combined.g.vcf.gz \
   -O rnaseq/vcf/pk15_combined.vcf.gz \
   --standard-min-confidence-threshold-for-calling 20


echo Done