#!/bin/bash
#$ -N find_low_coverage
#$ -cwd
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G

set -eu

. /etc/profile.d/modules.sh

module load igmm/apps/R/3.5.0


if [ ! -d outputs/low_coverage_regions ]; then
    mkdir outputs/low_coverage_regions
fi


for CHR in {1..18} X Y;
do

    Rscript R/find_low_coverage.R \
	    2 \
	    outputs/coverage_chr${CHR}.bedgraph \
	    outputs/low_coverage_regions/low_coverage_regions_chr${CHR}.bedgraph
    
done

cat outputs/low_coverage_regions/low_coverage_regions_chr*.bedgraph > \
    outputs/low_coverage_regions.bed

echo Done
