#!/bin/bash
#$ -cwd
#$ -N ips_genotype_gvcf
#$ -l h_rt=48:00:00
#$ -l h_vmem=64G


. /etc/profile.d/modules.sh

set -ue

source scripts/set_tools_paths.sh

if [ ! -d seq/genotype_gvcf ]; then
    mkdir seq/genotype_gvcf
fi


java -Xmx100g -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T GenotypeGVCFs \
-R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
--variant seq/gvcf/ips.g.vcf \
-o seq/genotype_gvcf/ips.vcf
