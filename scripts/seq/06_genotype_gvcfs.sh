#!/bin/bash
#$ -cwd
#$ -N pk15_genotype_gvcf
#$ -l h_rt=200:00:00
#$ -l h_vmem=128G


## Genotype GW and EG vcf files rel to the reference genome

. /etc/profile.d/modules.sh

set -ue

source scripts/set_tools_paths.sh

if [ ! -d seq/genotype_gvcf ]; then
    mkdir seq/genotype_gvcf
fi


java -Xmx100g -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T GenotypeGVCFs \
-R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
--variant seq/gvcf/EG_pk15.g.vcf \
--variant seq/gvcf/GW_pk15.g.vcf \
-o seq/genotype_gvcf/pk15_genotype.vcf
