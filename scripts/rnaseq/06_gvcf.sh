#!/bin/bash
#$ -cwd
#$ -N gvcf
#$ -l h_rt=48:00:00
#$ -l h_vmem=32G

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d rnaseq/gvcf ]; then
  mkdir rnaseq/gvcf
fi

$GATK4_HOME/gatk HaplotypeCaller  \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -I rnaseq/recal/pk15_bodymap_recal.bam \
   -O rnaseq/gvcf/pk15_bodymap.g.vcf.gz \
   -ERC GVCF
   
$GATK4_HOME/gatk HaplotypeCaller  \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -I rnaseq/recal/pk15_mock_recal.bam \
   -O rnaseq/gvcf/pk15_mock.g.vcf.gz \
   -ERC GVCF

echo Done