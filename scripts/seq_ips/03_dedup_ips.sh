#!/bin/bash
#$ -cwd
#$ -N ips_dedup
#$ -l h_rt=24:00:00
#$ -l h_vmem=16G

## Use picard to mark duplicate alignments; also collect alignment statistics

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq/dedup ]; then
    mkdir seq/dedup
fi

cd seq

java -Xmx4g -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
     I=aln/ips.bam \
     O=aln/ips_rg.bam \
     RGID=fibroblast \
     RGLB=fibroblast \
     RGPL=illumina \
     RGPU=fibroblast \
     RGSM=fibroblast \
     SORT_ORDER=coordinate \
     TMP_DIR=$TMPDIR

java -Xmx8g -jar $PICARD_HOME/picard.jar MarkDuplicates \
     I=aln/ips_rg.bam \
     O=dedup/ips_dedup.bam \
     M=dedup/ips_dedup.txt \
     CREATE_INDEX=true TMP_DIR=$TMPDIR

java -Xmx8g -jar $PICARD_HOME/picard.jar CollectAlignmentSummaryMetrics \
     R=../genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     I=dedup/ips_dedup.bam \
     O=dedup/ips_alignment_metrics.txt
