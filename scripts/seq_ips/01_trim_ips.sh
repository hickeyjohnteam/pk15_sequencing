#!/bin/bash
#$ -cwd
#$ -N ips_trim
#$ -l h_rt=12:00:00
#$ -l h_vmem=8G


## Use Trimmomatic to remove adapter traces from reads

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq ]; then
    mkdir seq
fi

if [ ! -d seq/trim ]; then
    mkdir seq/trim
fi



java -Xmx4g -jar $TRIMMOMATIC_HOME/trimmomatic-0.36.jar PE -phred33 \
      /exports/cmvm/eddie/eb/groups/mJungnickel_grp/gWiz_fibroblast/Pig-fibroblast1_S36_R1_001.fastq.gz \
      /exports/cmvm/eddie/eb/groups/mJungnickel_grp/gWiz_fibroblast/Pig-fibroblast1_S36_R2_001.fastq.gz \
      seq/trim/ips_1_paired.fastq.gz \
      seq/trim/ips_1_unpaired.fastq.gz \
      seq/trim/ips_2_paired.fastq.gz \
      seq/trim/ips_2_unpaired.fastq.gz \
      ILLUMINACLIP:$TRIMMOMATIC_HOME/adapters/TruSeq3-PE.fa:2:30:10
