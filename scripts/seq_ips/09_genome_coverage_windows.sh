#!/bin/bash
#$ -cwd
#$ -N genome_coverage_windows_ips
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G



. /etc/profile.d/modules.sh

set -eu

module load roslin/bedtools/2.26.0
module load igmm/apps/samtools/1.6


if [ ! -d outputs/coverage ]; then
    mkdir outputs/coverage
fi

# 
# bedtools makewindows \
#     -g genomes/Sscrofa11.1_chr_sizes.txt \
#     -w 10000 > \
#     genomes/Sscrofa11.1_10kbp_windows.bed
# 

samtools view -uF 0x400 seq/base_recalibration/ips_BQSR.bam | \
bedtools coverage \
        -a genomes/Sscrofa11.1_10kbp_windows.bed \
        -b - \
        -sorted \
        > outputs/coverage/ips_coverage.txt


echo Done
