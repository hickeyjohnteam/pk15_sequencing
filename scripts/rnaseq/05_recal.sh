#!/bin/bash
#$ -cwd
#$ -N recal
#$ -l h_rt=24:00:00
#$ -l h_vmem=32G

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d rnaseq/recal ]; then
  mkdir rnaseq/recal
fi

$GATK4_HOME/gatk IndexFeatureFile \
     -I seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz
     
$GATK4_HOME/gatk IndexFeatureFile \
     -I seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz

$GATK4_HOME/gatk BaseRecalibrator \
   -I rnaseq/split/pk15_bodymap_split.bam \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   --known-sites seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz \
   --known-sites seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz \
   -O rnaseq/recal/pk15_bodymap_recal.table
   
$GATK4_HOME/gatk BaseRecalibrator \
   -I rnaseq/split/pk15_mock_split.bam \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   --known-sites seq/genotype_filtered_vcf/pk15_snps_BQSF_all_sorted.vcf.gz \
   --known-sites seq/genotype_filtered_vcf/pk15_indels_BQSF_all_sorted.vcf.gz \
   -O rnaseq/recal/pk15_mock_recal.table
   
$GATK4_HOME/gatk ApplyBQSR \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -I rnaseq/split/pk15_bodymap_split.bam \
   --bqsr-recal-file rnaseq/recal/pk15_bodymap_recal.table \
   -O rnaseq/recal/pk15_bodymap_recal.bam
  
$GATK4_HOME/gatk ApplyBQSR \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -I rnaseq/split/pk15_mock_split.bam \
   --bqsr-recal-file rnaseq/recal/pk15_mock_recal.table \
   -O rnaseq/recal/pk15_mock_recal.bam

echo Done