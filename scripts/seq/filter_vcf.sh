#!/bin/bash
#$ -cwd
#$ -N filter_vcf
#$ -l h_rt=200:00:00
#$ -l h_vmem=32G
#$ -pe sharedmem 4

## Use GATK to call the PK15 variants

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66
module load igmm/apps/vcftools/0.1.13

source scripts/set_tools_paths.sh


cd seq



## Use GATK to subset variants and apply hard filters

java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T SelectVariants \
     -R ../genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     -V vcf/pk15_raw.vcf \
     -o vcf/pk15_snps_raw.vcf \
     -selectType SNP


java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T SelectVariants \
     -R ../genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     -V vcf/pk15_raw.vcf \
     -o vcf/pk15_indels_raw.vcf \
     -selectType INDEL \
     -selectType MNP


java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T VariantFiltration \
     -R ../genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     -V vcf/pk15_snps_raw.vcf \
     --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" \
     --filterName "SNP_FILTERS" \
     -o vcf/pk15_snps_filters_applied.vcf
    
    
java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T VariantFiltration \
     -R ../genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     -V vcf/pk15_indels_raw.vcf \
     --filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0" \
     --filterName "INDEL_FILTERS" \
     -o vcf/pk15_indels_filters_applied.vcf

rm vcf/pk15_snps_raw.vcf
rm vcf/pk15_indels_raw.vcf


## Use vcftools to remove filtered variants and restrict to biallelic

vcftools --vcf vcf/pk15_snps_filters_applied.vcf \
	 --remove-filtered-all \
	 --recode \
	 --min-alleles 2 \
	 --max-alleles 2 \
	 --stdout > vcf/pk15_snps_filtered.vcf

vcftools --vcf vcf/pk15_indels_filters_applied.vcf \
	 --remove-filtered-all \
	 --recode \
	 --min-alleles 2 \
	 --max-alleles 2 \
	 --stdout > vcf/pk15_indels_filtered.vcf


rm vcf/pk15_snps_filters_applied.vcf
rm vcf/pk15_indels_filters_applied.vcf
