#!/bin/bash
#$ -cwd
#$ -N pk15_trim
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G


## Use Trimmomatic to remove adapter traces from reads

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq ]; then
    mkdir seq
fi

if [ ! -d seq/trim ]; then
    mkdir seq/trim
fi



java -Xmx4g -jar $TRIMMOMATIC_HOME/trimmomatic-0.36.jar PE -phred33 \
     ../X18203/2019-11-21/FD06234631/PK15_genomic_DNA_R1.fastq.gz \
     ../X18203/2019-11-21/FD06234631/PK15_genomic_DNA_R2.fastq.gz \
     seq/trim/EG_pk15_1_paired.fastq.gz \
     seq/trim/EG_pk15_1_unpaired.fastq.gz \
     seq/trim/EG_pk15_2_paired.fastq.gz \
     seq/trim/EG_pk15_2_unpaired.fastq.gz \
     ILLUMINACLIP:$TRIMMOMATIC_HOME/adapters/TruSeq3-PE.fa:2:30:10 


java -Xmx4g -jar $TRIMMOMATIC_HOME/trimmomatic-0.36.jar PE -phred33 \
	../40-355097144/MJ01/delivery/MJ01_Pig-PK15/MJ01_Pig-PK15_R1_001.fastq.gz \
  	../40-355097144/MJ01/delivery/MJ01_Pig-PK15/MJ01_Pig-PK15_R2_001.fastq.gz \
     seq/trim/GW_pk15_1_paired.fastq.gz \
     seq/trim/GW_pk15_1_unpaired.fastq.gz \
     seq/trim/GW_pk15_2_paired.fastq.gz \
     seq/trim/GW_pk15_2_unpaired.fastq.gz \
     ILLUMINACLIP:$TRIMMOMATIC_HOME/adapters/TruSeq3-PE.fa:2:30:10 
