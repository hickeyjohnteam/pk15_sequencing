#!/bin/bash
#$ -cwd
#$ -N pk15_trim
#$ -l h_rt=24:00:00
#$ -l h_vmem=8G


## Use Trimmomatic to remove adapter traces from Hampshire reads

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh


for RUN in ERR173174 ERR173175 ERR977248 ERR977249 ERR977250 ERR977251; do

    java -Xmx4g -jar $TRIMMOMATIC_HOME/trimmomatic-0.36.jar PE -phred33 \
        seq/fastq_ena/${RUN}_1.fastq.gz \
        seq/fastq_ena/${RUN}_2.fastq.gz \
        seq/trim/${RUN}_1_paired.fastq.gz \
        seq/trim/${RUN}_1_unpaired.fastq.gz \
        seq/trim/${RUN}_2_paired.fastq.gz \
        seq/trim/${RUN}_2_unpaired.fastq.gz \
        ILLUMINACLIP:$TRIMMOMATIC_HOME/adapters/TruSeq3-PE.fa:2:30:10 \
        ILLUMINACLIP:$TRIMMOMATIC_HOME/adapters/TruSeq2-PE.fa:2:30:10
        
done

echo Done