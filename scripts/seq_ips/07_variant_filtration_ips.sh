#!/bin/bash
#$ -cwd
#$ -N ips_variant_filtration
#$ -l h_rt=48:00:00
#$ -l h_vmem=32G

. /etc/profile.d/modules.sh

set -ue

source scripts/set_tools_paths.sh

if [ ! -d seq/genotype_filtered_vcf ]; then
    mkdir seq/genotype_filtered_vcf
fi

module load igmm/apps/jdk/1.8.0_66
module load igmm/apps/vcftools/0.1.13



java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T SelectVariants \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -V seq/genotype_gvcf/ips.vcf \
     -o seq/genotype_filtered_vcf/ips_snps_raw.vcf \
     -selectType SNP


java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T SelectVariants \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -V seq/genotype_gvcf/ips.vcf \
     -o seq/genotype_filtered_vcf/ips_indels_raw.vcf \
     -selectType INDEL \
     -selectType MNP


java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T VariantFiltration \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -V seq/genotype_filtered_vcf/ips_snps_raw.vcf \
     --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" \
     --filterName "SNP_FILTERS" \
     -o seq/genotype_filtered_vcf/ips_snps_filters_applied.vcf


java -Xmx24g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T VariantFiltration \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -V seq/genotype_filtered_vcf/ips_indels_raw.vcf \
     --filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0" \
     --filterName "INDEL_FILTERS" \
     -o seq/genotype_filtered_vcf/ips_indels_filters_applied.vcf


vcftools --vcf seq/genotype_filtered_vcf/ips_snps_filters_applied.vcf \
	 --remove-filtered-all \
	 --recode \
	 --stdout > seq/genotype_filtered_vcf/ips_snps.vcf


vcftools --vcf seq/genotype_filtered_vcf/ips_indels_filters_applied.vcf \
	 --remove-filtered-all \
	 --recode \
	 --stdout > seq/genotype_filtered_vcf/ips_indels.vcf
