#!/bin/bash
#$ -cwd
#$ -N filter_rnaseq
#$ -l h_rt=1:00:00
#$ -l h_vmem=16G

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

$GATK4_HOME/gatk VariantFiltration  \
   -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
   -V rnaseq/vcf/pk15_combined.vcf.gz \
   --window 35 \
   --cluster 3 \
   --filter-name "FS" \
   --filter "FS > 30.0" \
   --filter-name "QD" \
   --filter "QD < 2.0" \
   -O rnaseq/vcf/pk15_combined_filtered.vcf.gz
   
echo Done