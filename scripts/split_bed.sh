
set -eu


INPREFIX=${1:-}
OUTPREFIX=${2:-}
SUFFIX=${3:-}

for CHR in {1..18} X Y;
do

    grep  "^"$CHR$'\t' $INPREFIX.$SUFFIX > ${OUTPREFIX}_chr${CHR}.$SUFFIX
    
done


echo Done
