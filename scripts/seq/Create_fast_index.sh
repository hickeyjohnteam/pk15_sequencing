#!/bin/bash
#$ -cwd
#$ -N pk15_createfastindex
#$ -l h_rt=100:00:00
#$ -l h_vmem=16G



. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4
module load igmm/apps/samtools/1.3

source scripts/set_tools_paths.sh


java -Xmx10g -jar $PICARD_HOME/picard.jar CreateSequenceDictionary R= genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna  O= genomes/GCA_000003025.6_Sscrofa11.1_genomic.dict
samtools faidx genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna

