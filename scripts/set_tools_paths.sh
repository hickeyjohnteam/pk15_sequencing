
## Sequencing tools

SEQTOOLPATH=/exports/cmvm/eddie/eb/groups/mJungnickel_grp/martinj/tools/

GATK_HOME=$SEQTOOLPATH/GenomeAnalysisTK-3.5/
GATK4_HOME=$SEQTOOLPATH/gatk-4.3.0.0/
PICARD_HOME=$SEQTOOLPATH/picard-tools-2.9.0/
TRIMMOMATIC_HOME=$SEQTOOLPATH/Trimmomatic-0.36/


BCFTOOLS_HOME=/exports/cmvm/eddie/eb/groups/mJungnickel_grp/martinj/tools/bcftools-1.10.2/
