#!/bin/bash
#$ -cwd
#$ -N bcftools_rnaseq
#$ -l h_rt=1:00:00
#$ -l h_vmem=16G

. /etc/profile.d/modules.sh

set -ue

module load igmm/libs/htslib/1.4

source scripts/set_tools_paths.sh



#!/bin/bash
#$ -cwd
#$ -N genome_coverage_windows_ips
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G



. /etc/profile.d/modules.sh

set -eu

module load roslin/bedtools/2.26.0
module load igmm/apps/samtools/1.6


if [ ! -d outputs/coverage ]; then
    mkdir outputs/coverage
fi


bedtools makewindows \
     -g genomes/Sscrofa11.1_chr_sizes.txt \
     -w 10000 > \
     annotation/Sscrofa11.1_ensembl_10kbp_windows.bed

samtools view -uF 0x400 seq/base_recalibration/ips_BQSR.bam | \
bedtools coverage \
        -a genomes/Sscrofa11.1_10kbp_windows.bed \
        -b - \
        -sorted \
        > outputs/coverage/ips_coverage.txt


echo Done


$GATK4_HOME/gatk SelectVariants \
  -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
  -V rnaseq/vcf/pk15_combined_filtered.vcf.gz \
  --select-type-to-include SNP \
  --exclude-filtered \
  -O rnaseq/vcf/pk15_combined_snps_filtered_excluded.vcf.gz

$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
  rnaseq/vcf/pk15_combined_snps_filtered_excluded.vcf.gz > \
  outputs/pk15_rnaseq_snps_AD.txt

   
echo Done