#!/bin/bash
#$ -cwd
#$ -N pk15_EG_aln
#$ -l h_rt=200:00:00
#$ -l h_vmem=8G

## Use bwa mem to align reads to the reference genome

. /etc/profile.d/modules.sh

set -ue

source scripts/set_tools_paths.sh

if [ ! -d seq/aln ]; then
    mkdir seq/aln
fi


module load igmm/apps/bwa/0.7.12-r1039
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4
module load igmm/apps/samtools/1.3

bwa mem -M \
    genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
    seq/trim/EG_pk15_1_paired.fastq.gz \
    seq/trim/EG_pk15_2_paired.fastq.gz \
    | samtools view -Sbh - \
	       > seq/aln/EG_pk15.bam
     
     


