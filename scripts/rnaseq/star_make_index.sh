#!/bin/bash
#$ -cwd
#$ -N star_index
#$ -l h_rt=8:00:00
#$ -l h_vmem=64G


. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/STAR/2.7.8a

if [ ! -d rnaseq/star_index ]; then
  mkdir rnaseq/star_index
fi


if [ ! -f annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa ]; then
  cd annotation
  curl -O https://ftp.ensembl.org/pub/release-108/fasta/sus_scrofa/dna/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa.gz
  gunzip Sus_scrofa.Sscrofa11.1.dna.toplevel.fa.gz
  cd ..
fi

if [ ! -f annotation/Sus_scrofa.Sscrofa11.1.108.gtf ]; then
  cd annotation
  curl -O https://ftp.ensembl.org/pub/release-108/gtf/sus_scrofa/Sus_scrofa.Sscrofa11.1.108.gtf.gz
  gunzip Sus_scrofa.Sscrofa11.1.108.gtf.gz
  cd ..
fi


STAR --runThreadN 1 \
  --runMode genomeGenerate \
  --genomeDir rnaseq/star_index \
  --genomeFastaFiles annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
  --sjdbGTFfile annotation/Sus_scrofa.Sscrofa11.1.108.gtf \
  --sjdbOverhang 99
  
