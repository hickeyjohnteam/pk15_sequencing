#!/bin/bash
#$ -cwd
#$ -N download_ensembl
#$ -l h_rt=1:00:00
#$ -l h_vmem=2G


. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/tabix/0.2.6


cd annotation

## curl -O http://ftp.ensembl.org/pub/release-106/variation/vcf/sus_scrofa/sus_scrofa.vcf.gz

## curl -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/003/025/GCA_000003025.6_Sscrofa11.1/GCA_000003025.6_Sscrofa11.1_assembly_report.txt

gunzip sus_scrofa.vcf.gz

## Use grep to take away the header,  and keep the SNPs only,
## then awk to convert to bed
## (i.e. take the first column (chr), the second column (position) and
## subtract one to get the 0-based coordiante, and then the second
## column again to get the end of the interval).
grep -v ^\# sus_scrofa.vcf | \
  grep TSA\=SNV | \
  awk 'BEGIN {OFS="\t"}; { print $1, $2-1, $2; }' \
  > sus_scrofa.bed
