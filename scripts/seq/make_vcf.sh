#!/bin/bash
#$ -cwd
#$ -N make_vcf
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G
#$ -pe sharedmem 4

## Use GATK to call the PK15 variants

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66
module load roslin/samtools/1.9

source scripts/set_tools_paths.sh

if [ ! -d seq/vcf ]; then
    mkdir seq/vcf
fi

samtools faidx genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa

java -Xmx4g -jar $PICARD_HOME/picard.jar CreateSequenceDictionary \
     R=genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     O=genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.dict

cd seq


java -Xmx50g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T GenotypeGVCFs \
     -R ../genomes/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
     --variant gvcf/PK15_genomic_DNA.g.vcf.gz \
     -o vcf/pk15_raw.vcf
