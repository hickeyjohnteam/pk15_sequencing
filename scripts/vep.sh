#!/bin/bash
#$ -N vep
#$ -cwd
#$ -l h_rt=48:00:00 
#$ -l h_vmem=16G


set -eu

. /etc/profile.d/modules.sh


module load igmm/apps/vep/97


if [ ! -d outputs/vep ]; then
    mkdir outputs/vep
fi


vep -i seq/vcf/pk15_snps_filtered.vcf  \
    -o outputs/vep/snps.txt \
    --offline --cache --dir_cache ../tools/vep_cache/ \
    --merged \
    --species sus_scrofa \
    --sift s


vep -i seq/vcf/pk15_indels_filtered.vcf  \
    -o outputs/vep/indels.txt \
    --offline --cache --dir_cache ../tools/vep_cache/ \
    --merged \
    --species sus_scrofa \
    --sift s
