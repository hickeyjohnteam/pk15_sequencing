#!/bin/bash
#$ -cwd
#$ -N run_kallisto
#$ -l h_rt=24:00:00
#$ -l h_vmem=8G


. /etc/profile.d/modules.sh

set -ue

module load roslin/kallisto/0.44.0


kallisto quant \
  -i rnaseq/transcripts.idx \
  -o rnaseq/pk15_bodymap \
  -b 100 \
  ../pk15_rnaseq/SRR11939208_1.fastq.gz \
  ../pk15_rnaseq/SRR11939208_2.fastq.gz
  
  
kallisto quant \
  -i rnaseq/transcripts.idx \
  -o rnaseq/pk15_mock \
  -b 100 \
  ../pk15_rnaseq/SRR6806317_1.fastq.gz ../pk15_rnaseq/SRR6806317_2.fastq.gz \
  ../pk15_rnaseq/SRR6806316_1.fastq.gz ../pk15_rnaseq/SRR6806316_2.fastq.gz \
  ../pk15_rnaseq/SRR6806318_1.fastq.gz ../pk15_rnaseq/SRR6806318_2.fastq.gz



  