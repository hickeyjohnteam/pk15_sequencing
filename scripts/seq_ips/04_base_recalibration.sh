#!/bin/bash
#$ -cwd
#$ -N ips_recal
#$ -l h_rt=72:00:00
#$ -l h_vmem=8G

## Use base recalibration to remove bias from quality scores

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/R/3.5.0

source scripts/set_tools_paths.sh

if [ ! -d seq/base_recalibration ]; then
    mkdir seq/base_recalibration
fi


java -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T BaseRecalibrator \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -I seq/dedup/ips_dedup.bam \
     -knownSites annotation/sus_scrofa_genbank_chrs.bed \
     -o seq/base_recalibration/ips_recalibration_firstpass.table

java -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T BaseRecalibrator \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -I seq/dedup/ips_dedup.bam \
     -knownSites annotation/sus_scrofa_genbank_chrs.bed \
     -BQSR seq/base_recalibration/ips_recalibration_firstpass.table \
     -o seq/base_recalibration/ips_recalibration_secondpass.table

java -jar $GATK_HOME/GenomeAnalysisTK.jar \
  -T PrintReads \
  -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
  -I seq/dedup/ips_dedup.bam \
  -BQSR seq/base_recalibration/ips_recalibration_firstpass.table \
  -o seq/base_recalibration/ips_BQSR.bam

java -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T AnalyzeCovariates \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -before seq/base_recalibration/ips_recalibration_firstpass.table \
     -after seq/base_recalibration/ips_recalibration_secondpass.table \
     -csv seq/base_recalibration/ips_BQSR.csv \
     -plots seq/base_recalibration/ips_BQSR.pdf
