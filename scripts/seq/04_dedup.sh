#!/bin/bash
#$ -cwd
#$ -N pk15_dedup
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G

## Use picard to mark duplicate alignments; also collect alignment statistics

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [! -d seq/dedup ]; then
    mkdir seq/dedup
fi

cd seq

java -Xmx4g -jar $PICARD_HOME/picard.jar MarkDuplicates \
     I=aln/pk15_rg.bam \
     O=dedup/pk15_dedup.bam \
     M=dedup/pk15_dedup.txt \
     CREATE_INDEX=true TMP_DIR=$TMPDIR

java -Xmx4g -jar $PICARD_HOME/picard.jar CollectAlignmentSummaryMetrics \
     R=../genomes/Sscrofa11.fasta \
     I=dedup/pk15_dedup.bam \
     O=dedup/pk15_alignment_metrics.txt
