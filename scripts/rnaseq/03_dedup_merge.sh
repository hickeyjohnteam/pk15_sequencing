#!/bin/bash
#$ -cwd
#$ -N dedup_star
#$ -l h_rt=24:00:00
#$ -l h_vmem=32G


. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d rnaseq/dedup ]; then
    mkdir rnaseq/dedup
fi

cd rnaseq

java -Xmx4g -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
     I=star_SRR11939208Aligned.sortedByCoord.out.bam \
     O=SRR11939208_rg.bam \
     RGID=pk15_bodymap \
     RGLB=pk15_bodymap \
     RGPL=illumina \
     RGPU=pk15_bodymap \
     RGSM=pk15_bodymap \
     SORT_ORDER=coordinate \
     TMP_DIR=$TMPDIR

java -Xmx8g -jar $PICARD_HOME/picard.jar MarkDuplicates \
     I=SRR11939208_rg.bam \
     O=dedup/pk15_bodymap_dedup.bam \
     M=dedup/pk15_bodymap_dedup.txt \
     CREATE_INDEX=true TMP_DIR=$TMPDIR

rm SRR11939208_rg.bam


for RUN in SRR6806316 SRR6806317 SRR6806318; do

  java -Xmx4g -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
     I=star_${RUN}Aligned.sortedByCoord.out.bam \
     O=${RUN}_rg.bam \
     RGID=pk15_${RUN} \
     RGLB=pk15_${RUN} \
     RGPL=illumina \
     RGPU=pk15_${RUN} \
     RGSM=pk15_mock \
     SORT_ORDER=coordinate \
     TMP_DIR=$TMPDIR

  java -Xmx8g -jar $PICARD_HOME/picard.jar MarkDuplicates \
     I=${RUN}_rg.bam \
     O=dedup/${RUN}_dedup.bam \
     M=dedup/${RUN}_dedup.txt \
     CREATE_INDEX=true TMP_DIR=$TMPDIR

  rm ${RUN}_rg.bam

done


java -jar $PICARD_HOME/picard.jar MergeSamFiles \
      I=dedup/SRR6806316_dedup.bam \
      I=dedup/SRR6806317_dedup.bam \
      I=dedup/SRR6806318_dedup.bam \
      O=dedup/pk15_mock_dedup.bam
      
rm dedup/SRR6806316_dedup.bam
rm dedup/SRR6806317_dedup.bam
rm dedup/SRR6806318_dedup.bam

echo Done