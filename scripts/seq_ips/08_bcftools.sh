#!/bin/bash
#$ -N ips_bcf
#$ -cwd
#$ -l h_rt=8:00:00
#$ -l h_vmem=16G


## bcftools and htslib module (for bgzip)

BCFTOOLS_HOME=/exports/cmvm/eddie/eb/groups/mJungnickel_grp/martinj/tools/bcftools-1.10.2/

. /etc/profile.d/modules.sh

module load igmm/libs/htslib/1.4

if [ ! -d outputs ]; then
  mkdir outputs
fi

## Block zip, sort and index
bgzip seq/genotype_filtered_vcf/ips_snps.vcf
$BCFTOOLS_HOME/bcftools sort seq/genotype_filtered_vcf/ips_snps.vcf.gz \
-O z > seq/genotype_filtered_vcf/ips_snps_sorted.vcf.gz
$BCFTOOLS_HOME/bcftools index seq/genotype_filtered_vcf/ips_snps_sorted.vcf.gz

bgzip seq/genotype_filtered_vcf/ips_indels.vcf
$BCFTOOLS_HOME/bcftools sort seq/genotype_filtered_vcf/ips_indels.vcf.gz \
-O z > seq/genotype_filtered_vcf/ips_indels_sorted.vcf.gz
$BCFTOOLS_HOME/bcftools index seq/genotype_filtered_vcf/ips_indels_sorted.vcf.gz

## Query for per-sample tags
$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
seq/genotype_filtered_vcf/ips_snps_sorted.vcf.gz > \
outputs/ips_snps_AD.txt

$BCFTOOLS_HOME/bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD]\n' \
seq/genotype_filtered_vcf/ips_indels_sorted.vcf.gz > \
outputs/ips_indels_AD.txt
