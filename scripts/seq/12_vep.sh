#!/bin/bash
#$ -N vep
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=16G


set -eu

. /etc/profile.d/modules.sh


module load roslin/vep/99.2


if [ ! -d outputs/vep ]; then
    mkdir -p outputs/vep
fi


vep -i seq/genotype_filtered_vcf/pk15_snps_BQSF_all.vcf  \
    -o outputs/vep/snps.txt \
    --offline --cache --dir_cache vep_cache \
    --merged \
    --species sus_scrofa \
    --sift s


vep -i seq/genotype_filtered_vcf/pk15_indels_BQSF_all.vcf  \
    -o outputs/vep/indels.txt \
    --offline --cache --dir_cache vep_cache \
    --merged \
    --species sus_scrofa \
    --sift s
