#!/bin/bash
#$ -N vep_to_bed
#$ -cwd
#$ -l h_rt=8:00:00
#$ -l h_vmem=64G

set -eu

. /etc/profile.d/modules.sh

module load igmm/apps/R/3.5.0


if [ ! -d outputs/vep_bed ]; then
    mkdir outputs/vep_bed
fi


Rscript R/vep_to_bed.R
    

echo Done
