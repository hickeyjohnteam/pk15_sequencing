#!/bin/bash
#$ -cwd
#$ -N genome_coverage
#$ -l h_rt=48:00:00
#$ -l h_vmem=16G

## Get sequence coverage of whole genome of the PK15 genome


. /etc/profile.d/modules.sh

set -eu

module load roslin/bedtools/2.26.0


if [ ! -d outputs ]; then
    mkdir outputs
fi

bedtools genomecov -ibam seq/bam/PK15_genomic_DNA.bam -bga > outputs/coverage.bedgraph


for CHR in {1..18} X Y;
do

    grep  "^"$CHR$'\t' outputs/coverage.bedgraph > outputs/coverage_chr${CHR}.bedgraph
    
done


echo Done
