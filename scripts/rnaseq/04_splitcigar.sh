#!/bin/bash
#$ -cwd
#$ -N split_cigar
#$ -l h_rt=24:00:00
#$ -l h_vmem=32G

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66
module load igmm/apps/samtools/1.6

source scripts/set_tools_paths.sh

if [ ! -d rnaseq/split ]; then
  mkdir rnaseq/split
fi

$GATK4_HOME/gatk SplitNCigarReads \
      -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
      -I rnaseq/dedup/pk15_bodymap_dedup.bam \
      -O rnaseq/split/pk15_bodymap_split.bam \
      --tmp-dir $TMPDIR
      
$GATK4_HOME/gatk SplitNCigarReads \
      -R annotation/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
      -I rnaseq/dedup/pk15_mock_dedup.bam \
      -O rnaseq/split/pk15_mock_split.bam \
      --tmp-dir $TMPDIR


echo Done