#!/bin/bash
#$ -cwd
#$ -N pk15_GW_gvcf
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 16

## Use GATK haplotype caller to create gvcf

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq/gvcf ]; then
    mkdir seq/gvcf
fi


java -Xmx100g -jar $GATK_HOME/GenomeAnalysisTK.jar \
     -T HaplotypeCaller \
     -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     -I seq/dedup/GW_pk15_dedup.bam \
     --emitRefConfidence GVCF \
     -variant_index_type LINEAR \
     -variant_index_parameter 128000 \
     -nct 16 \
     -o seq/gvcf/GW_pk15.g.vcf


