#!/bin/bash
#$ -cwd
#$ -N pk15_dedup
#$ -l h_rt=100:00:00
#$ -l h_vmem=8G

## Use picard to mark duplicate alignments; also collect alignment statistics

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/jdk/1.8.0_66

source scripts/set_tools_paths.sh

if [ ! -d seq/dedup ]; then
    mkdir seq/dedup
fi


java -Xmx4g -jar $PICARD_HOME/picard.jar MarkDuplicates \
     I=seq/aln/GW_pk15_rg.bam \
     O=seq/dedup/GW_pk15_dedup.bam \
     M=seq/dedup/GW_pk15_dedup.txt \
     CREATE_INDEX=true TMP_DIR=$TMPDIR

java -Xmx4g -jar $PICARD_HOME/picard.jar CollectAlignmentSummaryMetrics \
     R=genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
     I=seq/dedup/GW_pk15_dedup.bam \
     O=seq/dedup/GW_pk15_alignment_metrics.txt


