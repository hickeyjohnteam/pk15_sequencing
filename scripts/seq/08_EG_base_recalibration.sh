#!/bin/bash
#$ -cwd
#$ -N pk15_EG_BQSR
#$ -l h_rt=200:00:00
#$ -l h_vmem=16G

## Use base recalibration to remove bias from quality scores

. /etc/profile.d/modules.sh

set -ue

module load igmm/apps/R/3.5.0

source scripts/set_tools_paths.sh

if [ ! -d seq/base_recalibration ]; then
    mkdir seq/base_recalibration

fi


# Generate the first pass recalibration table file
 java -jar $GATK_HOME/GenomeAnalysisTK.jar \
      -T BaseRecalibrator \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -I seq/dedup/EG_pk15_dedup.bam \
      -knownSites seq/genotype_filtered_vcf/pk15_snps_all.vcf \
      -knownSites seq/genotype_filtered_vcf/pk15_indels_all.vcf \
      -o seq/base_recalibration/EG_recalibration_firstpass.table


 # Generate the second pass recalibration table file
 java -jar $GATK_HOME/GenomeAnalysisTK.jar \
      -T BaseRecalibrator \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -I seq/dedup/EG_pk15_dedup.bam \
      -knownSites seq/genotype_filtered_vcf/pk15_snps_all.vcf \
      -knownSites seq/genotype_filtered_vcf/pk15_indels_all.vcf \
      -BQSR seq/base_recalibration/EG_recalibration_firstpass.table \
      -o seq/base_recalibration/EG_recalibration_secondpass.table


 # Finally generate the plots and also keep a copy of the csv (optional)
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
   -T PrintReads \
   -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
   -I seq/dedup/EG_pk15_dedup.bam \
   -BQSR seq/base_recalibration/EG_recalibration_firstpass.table \
   -o seq/base_recalibration/EG_BQSR.bam


 java -jar $GATK_HOME/GenomeAnalysisTK.jar \
      -T AnalyzeCovariates \
      -R genomes/GCA_000003025.6_Sscrofa11.1_genomic.fna \
      -before seq/base_recalibration/EG_recalibration_firstpass.table \
      -after seq/base_recalibration/EG_recalibration_secondpass.table \
      -csv seq/base_recalibration/EG_BQSR.csv \
      -plots seq/base_recalibration/EG_BQSR.pdf
