#!/bin/bash
#$ -cwd
#$ -N run_kallisto
#$ -l h_rt=8:00:00
#$ -l h_vmem=8G


. /etc/profile.d/modules.sh

set -ue

module load roslin/kallisto/0.44.0


kallisto quant \
  -i rnaseq/transcripts.idx \
  -o rnaseq/fibroblast \
  -b 100 \
  ../../gWiz_fibroblast/Pig-fibroblast1_S36_R1_001.fastq.gz \
  ../../gWiz_fibroblast/Pig-fibroblast1_S36_R2_001.fastq.gz
  