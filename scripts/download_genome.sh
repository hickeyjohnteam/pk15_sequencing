#!/bin/bash
#$ -cwd
#$ -N download_genome
#$ -l h_rt=2:00:00
#$ -l h_vmem=8G

. /etc/profile.d/modules.sh
module load igmm/apps/bwa/0.7.12-r1039
set -eu

if [ ! -d genomes ]; then
    mkdir genomes
fi

cd genomes

if [ ! -f GCA_000003025.6_Sscrofa11.1_genomic.fna ]; then
	curl -O https://ftp.ncbi.nlm.nih.gov/genomes/genbank/vertebrate_mammalian/Sus_scrofa/latest_assembly_versions/GCA_000003025.6_Sscrofa11.1/GCA_000003025.6_Sscrofa11.1_genomic.fna.gz

	gunzip GCA_000003025.6_Sscrofa11.1_genomic.fna.gz
fi

bwa index GCA_000003025.6_Sscrofa11.1_genomic.fna